# Annex 4: Data formats

## 3.1.3.1: Data formats

### Requirements

* Disclosure of data formats
  * Submitting the manuals or technical data sheets in which the data formats are documented **_or_**
  * Providing examples of other software products (from other suppliers) that can process these data formats **_or_**
  * Stating the data formats and assigning them to an open standard.

### Documentation of data formats

Supported data formats are documented at https://okular.kde.org/formats.

Here are the specifications for the supported formats:

* [PDF](https://www.iso.org/standard/51502.html)
* [PostScript](https://www.adobe.com/content/dam/acom/en/devnet/actionscript/articles/PLRM.pdf)
* [TIFF](https://www.adobe.io/open/standards/TIFF.html)
* [CHM](http://www.russotto.net/chm/chmformat.html)
* [DjVu](http://www.djvu.org/)
* Various image formats ([JPEG](https://jpeg.org/jpeg/index.html), [PNG](https://www.w3.org/TR/2003/REC-PNG-20031110/), etc.)
* [DVI](https://ctan.org/pkg/dvitype)
* [XPS](https://www.ecma-international.org/publications-and-standards/standards/ecma-388/)
* [ODT](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office)
* [Fiction Book](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office)
* [Comic Book](https://en.wikipedia.org/wiki/Comic_book_archive)
* [Plucker](http://www.fifi.org/doc/plucker/manual/DBFormat.html)
* [EPUB](https://www.w3.org/publishing/epub3/epub-spec.html)
* [Mobipocket](https://www.loc.gov/preservation/digital/formats/fdd/fdd000472.shtml)

Exact details of each format are fully available for inspection because the source code of its implementation is available as open source.
