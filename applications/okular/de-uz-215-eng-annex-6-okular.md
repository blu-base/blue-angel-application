# Annex 6: Product information

## 3.1.3.3: Continuity of the software product

### Requirements

* Security updates must be provided free of charge
* Security updates for the labelled product for at least 5 years after the end of sale
* The user must be given the option of whether to install only security updates or also other (e.g. functional) updates.

### Product information in which fullfilement of requirements is documented

Okular has a history of continuous incremental updates as open source product for 15 years starting in 2006. See [full list of release announcements](https://okular.kde.org/news/). All updates always have been and always will be released free of charge.

Okular is released as part of the collection of KDE application the KDE project releases on a [4 month release schedule](https://community.kde.org/Schedules). From April 2021 on this set is called "KDE Gear". In the years before it was mainly known as "KDE Applications". This release schedule is maintained by the KDE community and supported by KDE e.V. The community is committed to doing this indefinitely.

As the source code is released as open source, it always will be possible for others to pick up Okular releases and continue them. Okular is shipped by many Linux distributions which give a variety of different support levels. Some distributions offer multi-year plans. See also the [packaging status](https://okular.kde.org/download/).

The openness of the code, the proven sustainable community and organization behind it, which by its nature is more independent of economic challenges than commercial companies, and the diversity of third parties vendors which distribute Okular in various forms guarantee Okular's continuity as a software product over a long period of time, going way beyond a 5 year perspective.

The continuous incremental releases make sure users reliably get a working product with latest security updates. This includes the changes necessary to adapt to releases of dependencies which fix security related problems. Functional changes are incremental and it wouldn't serve users to separate them from pure security updates becuase users would be blocked from critical fixes and security updates of dependencies.

Because of the code being released as open source code users also always have the option to maintain their own branch of Okular, backport the fixes they need or add their own ones. This could also be done by one of the many distributors who ship versions of Okular for the users who are not capable or willing to do it themselves.

In any case continuity is guaranteed in principle forever by the chosen open source license.

## 3.1.3.4: Uninstallability

### Requirement

* Residue-free uninstallability of the software

### Product information in which fullfilement of requirements is documented

Okular can be uninstalled without leaving residues on the system. The exact procedure depends on how it has been installed.

When installing from sources, cmake produces a list of files to delete which can be done with a simple shell script. Users who install from sources know how to do that.

When installing binary packages, the instructions of the package provider apply. With Linux distributions there usually is a command line tool which has some kind of uninstall command. They also usually offer graphical applications to uninstall packages.

For example the uninstall instructions for the Flatpak package manager which is linked on the Okular Website on the [download page](https://okular.kde.org/download/), are in the [Flatpak documentation](https://docs.flatpak.org/en/latest/using-flatpak.html#remove-an-application).

The data Okular handles is completely separate from the program. It's under full control of the user. An uninstallation won't touch any user-generated data.

## 3.1.3.5: Offline capability

### Requirement

* The functionality and availability of the software must not be negatively influenced by external factors, such as the availability of a licence server.

### Product information in which fullfilement of requirements is documented

Okular is fully capable of offline usage. No network connections are required for any operations other than the user explicitly specifying a document to be retrieved over the network. Because Okular doesn't require network connections in any way, there is no specific documentation needed.

## 3.1.3.6: Modularity

### Requirements

* Information on how individual modules of the software product can be deactivated during the installation process.
* Information on the extent to which individual modules of the software product (especially those that do not belong to the functions of the software product such as tracking, etc.) can be deactivated during the use of the software product.

### Product information in which fullfilement of requirements is documented

Okular is a single purpose application as a document viewer. It's mostly used as a modular part of a desktop, often the KDE Plasma desktop, but also desktops on other platforms. It doesn't have extra modules which are not related to its core functionality, such as tracking or cloud integration, etc.

When installing from source it can be configured for the platform it's intended to run on and optional functionality is only built if the corresponding dependencies such as a text-to-speech system are available. Technical details are made available in the [build control file](https://invent.kde.org/graphics/okular/-/blob/master/CMakeLists.txt).

When Okular is installed from packages built by Linux distributions or other packagers, they usually are built in a modular way, so that for example translations of the user interface or the manuals can be installed separately, in the configuartion chosen by the user. Example: [openSUSE](https://en.opensuse.org/Okular#Modules).

## 3.1.3.8: Documentation of the software product, licence conditions and terms of use

### Requirement

Information about the software product both publicly and also in combination with the product itself:

a) Description of the processes for installing and uninstalling the software
b) Description of the data import and export processes
c) Information on reducing the use of resources
d) Information on the licensing terms and terms of use to enable, where relevant, the legally compliant further development of the software product
e) Information on software support
f) Information on the handling of data, in the sense of existing data protection laws
g) Information on data security, data collection and data transmission

### Product information in which fullfilement of requirements is documented

a) Description of the processes for installing and uninstalling the software

Okular can be installed in several ways, depending on preferences of users and which platforms they use. There is a detailed overview of installation options: https://okular.kde.org/download. Specific instructions for installing and uninstalling are part of the instructions and documentation of the general platforms used to handle the specific Okular packages chosen by users. Okular gives users full control here by enabling them to chose their preferred installation mechanism. Generic documentation which is applicable to Okular is available as [tutorial for installing KDE applications](https://userbase.kde.org/Tutorials/Install_KDE_software) on [KDE UserBase](https://userbase.kde.org)

b) Description of the data import and export processes

As a document reader Okular is mostly about reading a variety of document formats. A detailed list of formats and specific capabilites supported for each of them is documented: https://okular.kde.org/formats/. In the vast majority of use cases data is not changed or created, so no export is necessary. In the few cases where data is changed (e.g. annotating a PDF file) it uses the standard functionality for saving document data which is well known by users.

c) Information on reducing the use of resources

Okular is designed to make effective use of resources. It focuses on the core functionality of viewing documents and doesn't include any functionality which would consume resources going beyond what is necessary to serve this core purpose. Information how some configuration parameters can affect performance and resource usage are documented in the manual: https://docs.kde.org/stable5/en/okular/okular/configperformance.html.

d) Information on the licensing terms and terms of use to enable, where relevant, the legally compliant further development of the software product

Okular is Free Software and released under the GNU Public License which allows unlimited use and modifications and distribution of changes as long as this is done under the GPL as well. This is stated in the "Free Software" section on the web site: https://okular.kde.org/. The source code for Okular is publically available: https://invent.kde.org/graphics/okular. The full text of the license is part of the code and is required to be part of any distribution of Okular: https://invent.kde.org/graphics/okular/-/blob/master/COPYING. The license is also stated in the Okular manual: https://docs.kde.org/stable5/en/okular/okular/credits.html.

e) Information on software support

Okular is supported by the [KDE community](https://kde.org). Specific forums and points of contacts for Okular are available and documented: https://okular.kde.org/contact/.

f) Information on the handling of data, in the sense of existing data protection laws

There is no collection of personal data. Okular is fully compliant with existing data protection laws. The full privacy policy can be found here: https://kde.org/privacypolicy-apps/.

g) Information on data security, data collection and data transmission

Full documentation of data handling can be found in the KDE privacy policy: https://kde.org/privacypolicy-apps/. It also references the KDE Telemetry policy, which describes how telemetry data is collected in a way which fully protects user's privacy: https://community.kde.org/Policies/Telemetry_Policy.

Okular doesn't collect personal data and specifically doesn't transmit any data to other systems or parties. There are no ads, no user tracking. Users are always in full control of what they do with their data.

## 3.2.1: Requirements for the further development and update of the product

### Requirement

If the product is changed (e.g. through updates), it must be ensure that the software product still complies with all of the criteria.

### Product information in which fullfilement of requirements is documented

Future updates are not expected to change the product in a way which significantly affects any of the requirements.
