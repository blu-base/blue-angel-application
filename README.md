# Blue Angel Application

This repository contains the material required for applying for the [Blue Angel](https://www.blauer-engel.de/en/) label for KDE software. The Blue Angel is an ecolabel of the German federal Government which is awarded in a lot of different product categories. End of 2019 they introduced the [Blue Angel label for software](https://www.blauer-engel.de/en/products/electric-devices/resources-and-energy-efficient-software-products/resources-and-energy-efficient-software-products) and published [criteria](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-eng%20Criteria.pdf) based on energy consumption measurements and other sustainability aspects.

Our goal is to get the label for KDE applications, starting with Okular, Kate and KDevelop. As of January 2021 there are no listed products in the software category, so we could take a lead there. The criteria are free software friendly and currently only cover desktop software, so KDE is uniquely positioned to meet the requirements.

A big part of the criteria is the measurement of energy consumption. To get the data for this we created an open project to collect this data and drive more energy efficient software. The project is the [FOSS Energy Efficiency Project](https://invent.kde.org/cschumac/feep).

This repository tracks the effort to collect the data necessary for the application to certify our applications. The forms and supplemental information are listed here. Progress is tracked in the issues.

If you have any questions or comments please feel free to reach out to Cornelius Schumacher <schumacher@kde.org>.
