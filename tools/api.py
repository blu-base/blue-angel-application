#!/usr/bin/env python3

import requests
import os
from graphql import Mutation, Query

class Api:
    def __init__(self, endpoint, org, repo):
        self.endpoint = endpoint
        self.org = org
        self.repo = repo

    def request(self, json):
        if not "GITLAB_TOKEN" in os.environ or not os.environ["GITLAB_TOKEN"]:
            os.sys.exit("Environment variable GITLAB_TOKEN needs to be set to a GitLab authentication token")

        headers = {"Authorization": "bearer " + os.environ["GITLAB_TOKEN"]}
        r = requests.post(self.endpoint, headers=headers, json=json)

        if "errors" in r.json():
            raise Exception(r.json()["errors"])

        return r.json()["data"]

    def create_issue(self, title, description):
        escaped_description = description.replace('"', '\\"')

        m = Mutation()

        with m.field("createIssue", f"input: {{ projectPath: \"{self.org}/{self.repo}\", title: \"{title}\", description: \"{escaped_description}\" }}"):
            with m.field("issue"):
                m.field("id")
                m.field("iid")

        self.request(m.json())

    def get_issues(self):
        q = Query()
        with q.field("project", 'fullPath: "%s/%s"' % (self.org, self.repo)):
            q.field("name")
            args = "first: 50"
            with q.field("issues", args):
                with q.field("nodes"):
                    q.field("title")
                    q.field("description")
                    with q.field("labels", "first: 20"):
                        with q.field("nodes"):
                            q.field("title")

        return self.request(q.json())
