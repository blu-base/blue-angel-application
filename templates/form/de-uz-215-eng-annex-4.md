# Annex 4: Data formats

## 3.1.3.1: Data formats

### Requirements

* Disclosure of data formats
  * Submitting the manuals or technical data sheets in which the data formats are documented **_or_**
  * Providing examples of other software products (from other suppliers) that can process these data formats **_or_**
  * Stating the data formats and assigning them to an open standard.

### Documentation of data formats

